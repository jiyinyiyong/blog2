
Blog of JiyinYiyong
------

### About

Visit it here: http://blog.jiyinyiyong.info/

I'm trying to keep my notes about programming here.

### Installation

You need `node npm` and even `coffee-script forever` for this app.

I used to implement it on Linux servers and in Chrome.

```sh
cd your-path-for-apps/
git clone git@github.com:jiyinyiyong/blog2.git
cd blog2
npm install
forever start -c coffee app.coffee
```

Use `cake dev` in development.

### Thanks to:

http://nodejs.org/  
http://coffeescript.org/  
https://github.com/visionmedia/jade  
http://softwaremaniacs.org/media/soft/highlight/test.html  
http://kevinburke.bitbucket.org/markdowncss/  
http://www.google.com/webfonts/specimen/Inder  
http://www.google.com/webfonts/specimen/Oxygen

## Deploy

I'm trying to use Github Hooks to deploy this automaticly on VPS.

### License

MIT for the code.
