
## 尝试学习 Backbone

半年前姚指点我 Backbone 之前我就尝试过, 一直没能学会
到徐跟我讲解了面向对象, 终于有点明白 MVC 框架的意思
但完全没有练手的经验, 打算写, 但再次在模板的使用上卡住
通常的模板引擎非常冗余, Jade 在前端工具不那么自然

找到一个 Backbone 上使用 Jade 的用例, 先标记了吧
http://stackoverflow.com/questions/8528885/using-jade-templates-in-backbone-js
我后来发现在页面上写 Jade 的 Backbone 模板子集不会被转义
根据教程上的例子, 我大概确认了一下, 应该以后会去看
http://backbonetutorials.com/what-is-a-view/
