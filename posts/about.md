
Blog of JiyinYiyong
------

### intro

Here's my new notes

### ibout styles

For the styles, I adopted some design elements from [SVBTLE][svbtle].
I used [Markdown.css][markdown] to beautify Markdown elements.
And [Highlight.js][highlight] for code, both NPM module and css library.

[svbtle]: https://svbtle.com/
[markdown]: http://kevinburke.bitbucket.org/markdowncss/
[highlight]: http://softwaremaniacs.org/media/soft/highlight/test.html

------

更多我更早的博文请访问: http://jiyinyiyong.github.com/blog/
