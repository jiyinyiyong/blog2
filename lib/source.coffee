
fs = require 'fs'
show = console.log
path = require 'path'
mime = require "mime"

# show __dirname
exports.read = (url, res) ->
  filepath = path.join __dirname, '../', url
  fs.exists filepath, (exist) ->
    if exist
      fs.readFile filepath, "utf8", (err, file_content) ->
        res.writeHead 200, 'Content-Type': (mime.lookup url)
        res.end file_content
    else
      res.writeHead 404
      res.end()