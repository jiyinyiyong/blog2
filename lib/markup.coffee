
path = require 'path'
marked = require 'marked'
show = ->
willow = require 'willow'

color =require("highlight").Highlight

marked.setOptions
  gfm: yes
  sanitize: yes
  highlight: color
  breaks: yes

exports.markup = (file, url) ->
  extname = path.extname url
  if extname in ['.md', '.markdown']
    ret = marked file
    # show ret
    ret
  else if extname in ['.wl', '.willow']
    ret =  willow.willow file
    ret
  else file
