
fs = require 'fs'
jade = require 'jade'
show = console.log
path = require 'path'
markup = require('./markup').markup

# show __dirname
dir = {}
page = {}

dir.path = path.join __dirname, '../self/jade/dir.jade'
dir.tmpl = fs.readFileSync dir.path, 'utf8'
dir.html = jade.compile dir.tmpl, pretty: yes

page.path = path.join __dirname, '../self/jade/page.jade'
page.tmpl = fs.readFileSync page.path, 'utf8'
page.html = jade.compile page.tmpl, pretty: yes

index_paths = (url) ->
  # show "index_paths", url
  files = (url.split path.sep)[1...-1]
  # show files
  ret = []
  i = 0
  files.forEach (item) ->
    link = '/' + files[..i].join('/')
    # show link
    ret.push url: link, text: item
    i += 1
  # show "index nv", ret
  ret

index_files = (files, url) ->
  # show "index_files"
  dirname = path.dirname url
  ret = []
  files.sort().forEach (item) ->
    link = path.join dirname, item
    # show 'compare:: ', url, item
    if (path.basename url) is item then ret.push text: item
    else ret.push url: link, text: item
  ret

index_children = (url, callback) ->
  # show "index_children", url
  file_path = path.join __dirname, '../posts', url
  # show "index_children", file_path
  ret = []
  fs.readdir file_path, (err, children) ->
    children.forEach (item) ->
      ret.push url: (path.join url, item), text: item
    # show ret
    callback ret

exports.read = (url, res) ->
  file_path = path.join __dirname, '../posts/', url
  # show "path", file_path
  fs.exists file_path, (exist) ->
    unless exist
      res.writeHead 404
      res.end "not found #{file_path}"
    else
      # show "exist path", file_path
      fs.stat file_path, (err, stat) ->
        # show err, stat, file_path
        fs.readdir (path.dirname file_path), (err, files) ->
          # show files
          # show "use url", url

          obj =
            title: path.basename url
            paths: index_paths url
            files: index_files files, url

          res.writeHead 200, "Content-Type": "text/html"

          if stat.isDirectory()
            # show "isDirectory"
            index_children url, (ret) ->
              # show "ret"
              list = children: ret
              obj.html = dir.html list
              res.end (page.html obj)
          else
            fs.readFile file_path, 'utf8', (err, text) ->
              # show obj, file_path
              obj.html = markup text, url
              res.end (page.html obj)