
http = require 'http'
content = require './lib/content'
source = require './lib/source'
show = console.log

app = http.createServer (req, res) ->
  url = decodeURI req.url
  if url is '/'
    res.writeHead 301, Location: '/about.md'
    res.end()
  else if (url[..5] is '/self/') or (url is "/self")
    source.read url, res
  else
    content.read url, res

app.listen 3009
